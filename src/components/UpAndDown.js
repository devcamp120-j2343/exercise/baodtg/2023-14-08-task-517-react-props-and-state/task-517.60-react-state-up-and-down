import { Component } from "react";

class UpAndDown extends Component {
    onBtnDownClickHandler = () => {
        this.props.onBtnDownProps()
    }
    onBtnUpClickHandler = () => {
        this.props.onBtnUpProps()
    }
    render() {
        // let { resultProps, onBtnDownProps } = this.props;

        return (
            <>

                <button onClick={this.onBtnDownClickHandler}>Down</button>
                <p>{this.props.resultProps}</p>
                <button onClick={this.onBtnUpClickHandler}>Up</button>
            </>
        )
    }
}
export default UpAndDown