import { Component } from "react";
import UpAndDown from "./UpAndDown";

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            result: 0
        }
    }
    onBtnDownClick = () => {
        this.setState({
            result: this.state.result - 1
        })
    }
    onBtnUpClick = () => {
        this.setState({
            result: this.state.result + 1
        })
    }
    render() {
        return (
            <>
                <UpAndDown resultProps={this.state.result} onBtnDownProps={this.onBtnDownClick} onBtnUpProps={this.onBtnUpClick}/>
            </>
        )
    }
}
export default Content